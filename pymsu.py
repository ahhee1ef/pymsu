#!/usr/bin/env python3

import sys
import argparse

argument_parser = argparse.ArgumentParser(
	description = """Additional tool to work with TMSU tags.""",
	formatter_class = argparse.RawDescriptionHelpFormatter
)

subparsers = argument_parser.add_subparsers(dest = "command", metavar = "COMMAND")

mount_argument_parser = subparsers.add_parser(
	"mount",

	description = """Mounts a FUSE filesystem to view, edit, assign and remove TMSU tags.

By default, files and directories in the virtual filesystem appear owned by the real UID and GID of the FUSE process, and have its umask (access mode). This can be overridden by the corresponding FUSE options.""",

	help = "Mount a FUSE filesystem representing TMSU tags",

	formatter_class = argparse.RawDescriptionHelpFormatter
)

mount_argument_parser.add_argument(
	"--options", "-o",
	metavar = "OPTIONS",
	help = "mount options to pass to FUSE (see \"man mount.fuse\"). The option \"default_permissions\" is implied"
)

mount_argument_parser.add_argument(
	"TMSU_directory",
	metavar = "TMSU-DIRECTORY",
	help = "the directory with initialized TMSU (containing \".tmsu\")"
)

mount_argument_parser.add_argument(
	"mount_point",
	metavar = "MOUNT-POINT",
	help = "the directory to mount on"
)

arguments = argument_parser.parse_args()

if arguments.command is None:
	argument_parser.print_help()
elif arguments.command == "mount":
	import mount

	mount.main(arguments.TMSU_directory, arguments.mount_point, arguments.options)
