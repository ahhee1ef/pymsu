PyMSU
=====

Additional tool to work with [TMSU](https://github.com/oniony/TMSU/) tags. Implements tagging and untagging of files via moving them between directories in a virtual filesystem.

First, mount the filesystem (no root needed):

```bash
$ ./pymsu.py mount directory-with-tagged-files/ mount-point/
```

Then you can create new tags, delete existing ones, rename them by manipulating with virtual directories:

```bash
$ cd mount-point/labels/
$ mkdir new-tag
$ rmdir unneeded-tag/
$ rm -r non-empty-tag/
$ mv wrong-name/ correct-name
```

And you can operate with tagged files that are represented by symlinks in the tag directories:

```bash
$ cd one-tag/
$ ls
file.1.jpg  image.2.png
$ mv file.1.jpg ../another-tag/
$ rm image.2.png
```

But the main point of this tool is that you can do all these actions through a graphical file manager, so that the process of tagging and untagging of files doesn't differ too much from the usual process of sorting files between directories.

![Screenshot](screenshot.png)

Tags with values
----------------

Tags with values are supported, you can move files between them as with plain tags, but they are very fucked up, and I can do nothing about it. There are two limitations, inherited from TMSU:

1. Tags with values can't be empty, they live as long as there are files that bear them.
	- As a result, you can't create a directory named `tag=value`, because it would be initially empty.
	- When you delete the last file from a tag with value, it disappears (and `rm -r` returns an error, because it can't delete a directory that has already deleted itself).
2. A tag with value requires that a corresponding tag without value also exists.
	- When you delete multiple tags at once, you can get an error, because a file manager knows nothing about the rule, that tags with values must be deleted first, and only then the corresponding plain tags can be deleted.

Moreover, renaming of tags with values is not implemented, as well as renaming of plain tags for which there are corresponding tags with values.

Directories of tags with values are named like `tag=value`, so the equals sign is treated as a special character. If it appears inside a tag name or inside a value, it must be escaped.

Escaping
--------

There are three special characters:
- `/` is replaced with `%2f`, because it can't appear inside a directory name;
- `=` is replaced with `%3d` if it appears inside a tag name or value, but is left untouched if it's a separator between them;
- `%` itself is replaced with `%25`.
