import time
import stat
import re
import os.path
import collections
import errno
import logging
import os
import signal

import fuse

import logs
import database

# "Label" means a tag and value pair, or just a tag if it has no value.
# "Name" is either a tag or a value

def escape_name(name):
	result = ""

	for i in name:
		if i == "%":
			result += "%25"
		elif i == "=":
			result += "%3d"
		elif i == "/":
			result += "%2f"
		else:
			result += i

	return result

def unescape_name(escaped):
	result = ""

	i = 0

	while i < len(escaped):
		if escaped[i] == "%":
			code = escaped[i + 1: i + 3]

			if code == "25":
				result += "%"
			elif code == "3d":
				result += "="
			elif code == "2f":
				result += "/"
			else:
				return None

			i += 3
		else:
			result += escaped[i]

			i += 1

	return result

def escape_label(label):
	tag, value = label

	result = escape_name(tag)

	if value is not None:
		result += "=" + escape_name(value)

	return result

def unescape_label(escaped):
	parts = escaped.split("=")

	if len(parts) > 2:
		return None

	tag = unescape_name(parts[0])

	if tag is None or not database.validate_name(tag):
		return None

	value = None

	if len(parts) == 2:
		value = unescape_name(parts[1])

		if value is None or not database.validate_name(value):
			return None

	return tag, value

Symlink = collections.namedtuple("Symlink", ["content"])
Directory = collections.namedtuple("Directory", [])

labels_regexp = re.compile("/labels/[^/]+")
labels_file_regexp = re.compile("/labels/[^/]+/[^/]+")

def split_labels_path(path):
	escaped_label = path.split("/")[2]
	label = unescape_label(escaped_label)

	if label is None:
		return None

	return label

def split_labels_file_path(path):
	escaped_label, entry_name = path.split("/")[2: ]
	label = unescape_label(escaped_label)

	if label is None:
		return None

	return label, entry_name

def split_entry_name(entry_name):
	parts = entry_name.rsplit(".", maxsplit = 2)

	if len(parts) == 1 or not re.fullmatch("0|[1-9][0-9]*", parts[1]):
		return None

	file_name = ".".join(parts[: 1] + parts[2: ])
	ID = int(parts[1])

	if ID > 2 ** 63 - 1:
		return None

	return file_name, ID

fuse.fuse_python_api = 0, 2

class Server(fuse.Fuse):
	def __init__(self, TMSU_directory_name, mount_point_name, umask, UID, GID):
		super().__init__()

		self.TMSU_directory_name = os.path.abspath(TMSU_directory_name)
		self.mount_point_name = os.path.abspath(mount_point_name)

		self.umask = umask
		self.UID = UID
		self.GID = GID

		logging.info("TMSU directory: %r", self.TMSU_directory_name)
		logging.info("Mount point: %r", self.mount_point_name)

		self.database_file_name = os.path.join(self.TMSU_directory_name, ".tmsu/db")
		self.database = database.Database(self.database_file_name, self.TMSU_directory_name)

		self.multithreaded = False

	def fsinit(self):
		self.mount_time = time.clock_gettime_ns(time.CLOCK_REALTIME) / 1e9

		logging.info("Mount time: %f", self.mount_time)

	def fsdestroy(self):
		logging.info("Unmounted")

	# To ensure atomicity, every callback method must query the database only once,
	# or do a single call to the "_get_entry" function, which sometimes does the one query

	def _get_entry(self, path):
		if path == "/":
			return Directory()
		elif path == "/.database":
			return Symlink(self.database_file_name)
		elif path == "/labels":
			return Directory()
		elif labels_regexp.fullmatch(path):
			label = split_labels_path(path)

			if label is None:
				return None

			if self.database.check_label_exists(label):
				return Directory()
		elif labels_file_regexp.fullmatch(path):
			parts = split_labels_file_path(path)

			if parts is None:
				return None

			label, entry_name = parts

			parts = split_entry_name(entry_name)

			if parts is None:
				return None

			expected_file_name, ID = parts

			absolute_file_name = self.database.resolve_labeled_file(ID, label, expected_file_name)

			if absolute_file_name is None:
				return None

			return Symlink(absolute_file_name)

		return None

	@logs.log_calls
	def readlink(self, path):
		entry = self._get_entry(path)

		if entry is None:
			return -errno.ENOENT
		elif isinstance(entry, Symlink):
			return entry.content
		else:
			return -errno.EINVAL

	@logs.log_calls
	def getattr(self, path):
		entry = self._get_entry(path)

		if isinstance(entry, Directory):
			return fuse.Stat(
				st_nlink = 2,
				st_mode = stat.S_IFDIR | 0o777 & ~self.umask,
				st_uid = self.UID,
				st_gid = self.GID,
				st_atime = self.mount_time,
				st_mtime = self.mount_time,
				st_ctime = self.mount_time
			)
		elif isinstance(entry, Symlink):
			return fuse.Stat(
				st_nlink = 1,
				st_mode = stat.S_IFLNK | 0o777 & ~self.umask,
				st_uid = self.UID,
				st_gid = self.GID,
				st_atime = self.mount_time,
				st_mtime = self.mount_time,
				st_ctime = self.mount_time,
				st_size = len(entry.content)
			)

		return -errno.ENOENT

	@logs.log_calls
	def opendir(self, path):
		entry = self._get_entry(path)

		if entry is None:
			return -errno.ENOENT
		elif not isinstance(entry, Directory):
			return -errno.ENOTDIR

	@logs.log_calls
	def readdir(self, path, offset):
		yield fuse.Direntry("..", type = stat.S_IFDIR)
		yield fuse.Direntry(".", type = stat.S_IFDIR)

		if path == "/":
			yield fuse.Direntry("labels", type = stat.S_IFDIR)
			yield fuse.Direntry(".database", type = stat.S_IFLNK)
		elif path == "/labels":
			for i in self.database.get_labels():
				escaped_label = escape_label(i)

				yield fuse.Direntry(escaped_label, type = stat.S_IFDIR)
		elif labels_regexp.fullmatch(path):
			label = split_labels_path(path)

			if label is None:
				return

			for i in self.database.get_files_by_label(label):
				file_name, ID = i

				parts = file_name.rsplit(".", maxsplit = 1)
				entry_name = ".".join(parts[: 1] + [str(ID)] + parts[1: ])

				yield fuse.Direntry(entry_name, type = stat.S_IFLNK)

	@logs.log_calls
	def mkdir(self, path, mode):
		if labels_regexp.fullmatch(path):
			label = split_labels_path(path)

			if label is None:
				return -errno.EPERM

			result = self.database.create_label(label)

			if result == "already-exists":
				return -errno.EEXIST
			elif result == "value-present":
				return -errno.EPERM
		else:
			entry = self._get_entry(path)

			if entry is None:
				return -errno.EPERM

			return -errno.EEXIST

	@logs.log_calls
	def rmdir(self, path):
		if labels_regexp.fullmatch(path):
			label = split_labels_path(path)

			if label is None:
				return -errno.ENOENT

			result = self.database.delete_label(label)

			if result == "no-label":
				return -errno.ENOENT
			elif result == "files-labeled":
				return -errno.ENOTEMPTY
			elif result == "value-attached":
				return -errno.EPERM
			elif result == "implication-exists":
				return -errno.EPERM
		else:
			entry = self._get_entry(path)

			if entry is None:
				return -errno.ENOENT
			elif isinstance(entry, Directory):
				return -errno.EPERM
			else:
				return -errno.ENOTDIR

	@logs.log_calls
	def symlink(self, source, destination):
		if labels_file_regexp.fullmatch(destination):
			parts = split_labels_file_path(destination)

			if parts is None:
				return -errno.ENOENT

			label, entry_name = parts

			parts = split_entry_name(entry_name)

			if parts is None:
				return -errno.EPERM

			expected_file_name, ID = parts

			absolute_source = os.path.normpath(os.path.join(
				self.mount_point_name,
				"labels",
				escape_label(label),
				source
			))

			result = self.database.label_file(ID, label, absolute_source)

			if result == "no-label":
				return -errno.ENOENT
			elif result == "no-file":
				return -errno.EPERM
			elif result == "already-labeled":
				return -errno.EEXIST
		else:
			entry = self._get_entry(destination)

			if entry is None:
				return -errno.EPERM

			return -errno.EEXIST

	@logs.log_calls
	def unlink(self, path):
		if labels_file_regexp.fullmatch(path):
			parts = split_labels_file_path(path)

			if parts is None:
				return -errno.ENOENT

			label, entry_name = parts

			parts = split_entry_name(entry_name)

			if parts is None:
				return -errno.ENOENT

			expected_file_name, ID = parts

			if not self.database.unlabel_file(ID, label, expected_file_name):
				return -errno.ENOENT
		else:
			entry = self._get_entry(path)

			if entry is None:
				return -errno.ENOENT
			elif isinstance(entry, Directory):
				return -errno.EISDIR
			else:
				return -errno.EPERM

	@logs.log_calls
	def rename(self, source, destination):
		if destination == source:
			return

		if labels_regexp.fullmatch(source) and labels_regexp.fullmatch(destination):
			source_label = split_labels_path(source)

			if source_label is None:
				return -errno.ENOENT

			destination_label = split_labels_path(destination)

			if destination_label is None:
				return -errno.EPERM

			result = self.database.rename_label(source_label, destination_label)

			if result == "no-source":
				return -errno.ENOENT
			elif result == "value-attached":
				return -errno.EPERM
			elif result == "value-present":
				return -errno.EPERM
			elif result == "unoverwritable-destination-exists":
				return -errno.ENOTEMPTY
		elif labels_file_regexp.fullmatch(source) and labels_file_regexp.fullmatch(destination):
			source_parts = split_labels_file_path(source)
			destination_parts = split_labels_file_path(destination)

			if source_parts is None or destination_parts is None:
				return -errno.ENOENT

			source_label, source_entry_name = source_parts
			destination_label, destination_entry_name = destination_parts

			if source_entry_name != destination_entry_name:
				return -errno.EPERM

			parts = split_entry_name(source_entry_name)

			if parts is None:
				return -errno.ENOENT

			expected_file_name, ID = parts

			if not self.database.relabel_file(ID, source_label, destination_label, expected_file_name):
				return -errno.ENOENT
		else:
			entry = self._get_entry(source)

			if entry is None:
				return -errno.ENOENT

			return -errno.EPERM

def main(TMSU_directory_name, mount_point_name, options):
	logs.initialize()

	umask = os.umask(0)
	os.umask(umask)

	UID = os.getuid()
	GID = os.getgid()

	logging.info("Detected umask: %03o, UID: %i, GID: %i", umask, UID, GID)

	prepended_options = "default_permissions,fsname=pymsu,subtype=pymsu"

	if options is not None:
		prepended_options += "," + options

	logging.info("Mount options: %r", prepended_options)

	def terminate(number, frame):
		os.kill(os.getpid(), signal.SIGTERM)

	signal.signal(signal.SIGINT, terminate)

	server = Server(TMSU_directory_name, mount_point_name, umask, UID, GID)
	server.main(["pymsu", "-f", "-s", "-o", prepended_options, mount_point_name])
