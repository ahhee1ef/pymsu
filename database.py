import unicodedata
import sqlite3
import textwrap
import os.path

def validate_name(name):
	if name in [
		".", "..",
		"and", "or", "not", "eq", "ne", "lt", "gt", "le", "ge",
		"AND", "OR", "NOT", "EQ", "NE", "LT", "GT", "LE", "GE"
	]:
		return False

	for i in name:
		category = unicodedata.category(i)

		if category[0] not in "LNPS" and category != "Zs":
			return False

	return True

class savepoint(object):
	def __init__(self, cursor):
		self.cursor = cursor

	def __enter__(self):
		self.cursor.execute("""SAVEPOINT "savepoint";""")

	def __exit__(self, type_, value, traceback):
		if type_ is None:
			self.cursor.execute("""RELEASE "savepoint";""")
		else:
			self.cursor.execute("""ROLLBACK TO "savepoint";""")

		return False

maximum_timeout_possible = (2 ** 31 - 1) / 1000

class Database(object):
	def __init__(self, file_name, TMSU_directory_name):
		self.file_name = file_name
		self.TMSU_directory_name = TMSU_directory_name

		self.connection = sqlite3.connect(self.file_name, maximum_timeout_possible, isolation_level = None)
		self.cursor = self.connection.cursor()

		self.cursor.execute("""PRAGMA "foreign_keys" = 'on';""")

		with savepoint(self.cursor):
			self.cursor.execute("""SELECT "major", "minor", "patch", "revision" FROM "version";""")

			version = self.cursor.fetchone()

			if version is None:
				raise Exception("Invalid database")

			if version < (0, 7, 0, 1):
				raise Exception("Database version is too old")

			# Superset of existing "idx_file_tag_tag_id"

			self.cursor.execute(textwrap.dedent("""
				CREATE INDEX IF NOT EXISTS "pymsu_index_1"
				ON "file_tag" ("tag_id", "value_id");
			"""))

	def _get_label_IDs(self, label):
		with savepoint(self.cursor):
			tag, value = label

			self.cursor.execute("""SELECT "id" FROM "tag" WHERE "name" == ?;""", (tag, ))

			row = self.cursor.fetchone()

			if row is None:
				return None

			tag_ID = row[0]

			if value is None:
				return tag_ID, 0

			self.cursor.execute("""SELECT "id" FROM "value" WHERE "name" == ?;""", (value, ))

			row = self.cursor.fetchone()

			if row is None:
				return None

			value_ID = row[0]

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "tag_id" == ? AND "value_id" == ?;
			""", (tag_ID, value_ID))

			if self.cursor.fetchone()[0] == 0:
				return None

			return tag_ID, value_ID

	def _get_file_by_ID(self, ID):
		with savepoint(self.cursor):
			self.cursor.execute("""SELECT "directory", "name" FROM "file" WHERE "id" == ?;""", (ID, ))

			row = self.cursor.fetchone()

			if row is None:
				return None

			return row

	def get_labels(self):
		with savepoint(self.cursor):
			self.cursor.execute("""SELECT "name" FROM "tag";""")

			for i in self.cursor:
				yield i[0], None

			self.cursor.execute("""
				SELECT DISTINCT "tag"."name", "value"."name"
				FROM "tag", "value", "file_tag"
				WHERE "tag"."id" == "file_tag"."tag_id" AND "value"."id" == "file_tag"."value_id";
			""")

			for i in self.cursor:
				yield i

	def check_label_exists(self, label):
		with savepoint(self.cursor):
			return self._get_label_IDs(label) is not None

	def get_files_by_label(self, label):
		with savepoint(self.cursor):
			label_IDs = self._get_label_IDs(label)

			if label_IDs is None:
				return

			tag_ID, value_ID = label_IDs

			self.cursor.execute("""
				SELECT "id", "name"
				FROM "file"
				WHERE "id" IN (
					SELECT "file_id"
					FROM "file_tag"
					WHERE "tag_id" == ? AND "value_ID" == ?
				);
			""", (tag_ID, value_ID))

			for i in self.cursor:
				ID, name = i

				yield name, ID

	def resolve_labeled_file(self, file_ID, label, expected_file_name):
		with savepoint(self.cursor):
			label_IDs = self._get_label_IDs(label)

			if label_IDs is None:
				return None

			tag_ID, value_ID = label_IDs

			row = self._get_file_by_ID(file_ID)

			if row is None:
				return None

			directory_name, file_name = row

			if file_name != expected_file_name:
				return None

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "file_id" == ? AND "tag_id" == ? AND "value_id" == ?;
			""", (file_ID, tag_ID, value_ID))

			if self.cursor.fetchone()[0] == 0:
				return None

			absolute_file_name = os.path.normpath(os.path.join(
				self.TMSU_directory_name,
				directory_name,
				expected_file_name
			))

			return absolute_file_name

	def create_label(self, label):
		with savepoint(self.cursor):
			tag, value = label

			label_IDs = self._get_label_IDs(label)

			if label_IDs is not None:
				return "already-exists"

			if value is not None:
				return "value-present"

			self.cursor.execute("""INSERT INTO "tag" ("name") VALUES (?);""", (tag, ))

			return "OK"

	def delete_label(self, label):
		with savepoint(self.cursor):
			label_IDs = self._get_label_IDs(label)

			if label_IDs is None:
				return "no-label"

			tag_ID, value_ID = label_IDs

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "tag_id" == ? AND "value_id" == ?;
			""", (tag_ID, value_ID))

			if self.cursor.fetchone()[0] > 0:
				return "files-labeled"

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "tag_id" == ? AND "value_id" != 0;
			""", (tag_ID, ))

			if self.cursor.fetchone()[0] > 0:
				return "value-attached"

			# No index for "implied_tag_id"!

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "implication"
				WHERE "tag_id" == ? OR "implied_tag_id" == ?;
			""", (tag_ID, tag_ID))

			if self.cursor.fetchone()[0] > 0:
				return "implication-exists"

			self.cursor.execute("""DELETE FROM "tag" WHERE "id" == ?;""", (tag_ID, ))

			return "OK"

	def rename_label(self, source, destination):
		with savepoint(self.cursor):
			source_tag, source_value = source
			destination_tag, destination_value = destination

			# Check the sourse existence first, not after the destination

			source_IDs = self._get_label_IDs(source)

			if source_IDs is None:
				return "no-source"

			source_tag_ID, source_value_ID = source_IDs

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "tag_id" == ? AND "value_id" != 0;
			""", (source_tag_ID, ))

			if self.cursor.fetchone()[0] > 0:
				return "value-attached"

			if destination_value is not None:
				return "value-present"

			destination_IDs = self._get_label_IDs(destination)

			if destination_IDs is not None and self.delete_label(destination) != "OK":
				return "unoverwritable-destination-exists"

			self.cursor.execute("""
				UPDATE "tag"
				SET "name" = ?
				WHERE "name" == ?;
			""", (destination_tag, source_tag))

			return "OK"

	def label_file(self, file_ID, label, expected_absolute_file_name):
		# Work around an original bug

		try:
			self.cursor.execute("""PRAGMA "foreign_keys" = 'off';""")

			with savepoint(self.cursor):
				label_IDs = self._get_label_IDs(label)

				if label_IDs is None:
					return "no-label"

				tag_ID, value_ID = label_IDs

				row = self._get_file_by_ID(file_ID)

				if row is None:
					return "no-file"

				directory_name, file_name = row

				absolute_file_name = os.path.normpath(os.path.join(
					self.TMSU_directory_name,
					directory_name,
					file_name
				))

				if absolute_file_name != expected_absolute_file_name:
					return "no-file"

				self.cursor.execute("""
					INSERT OR IGNORE INTO "file_tag" ("file_id", "tag_id", "value_id")
					VALUES (?, ?, ?);
				""", (file_ID, tag_ID, value_ID))

				if self.cursor.rowcount == 0:
					return "already-labeled"

				return "OK"
		finally:
			self.cursor.execute("""PRAGMA "foreign_keys" = 'on';""")

	def unlabel_file(self, file_ID, label, expected_file_name):
		with savepoint(self.cursor):
			label_IDs = self._get_label_IDs(label)

			if label_IDs is None:
				return False

			tag_ID, value_ID = label_IDs

			row = self._get_file_by_ID(file_ID)

			if row is None:
				return False

			directory_name, file_name = row

			if file_name != expected_file_name:
				return False

			self.cursor.execute("""
				DELETE FROM "file_tag"
				WHERE "file_id" == ? AND "tag_id" == ? AND "value_id" == ?;
			""", (file_ID, tag_ID, value_ID))

			if self.cursor.rowcount == 0:
				return False

			self.cursor.execute("""
				SELECT COUNT(*)
				FROM "file_tag"
				WHERE "file_id" == ?;
			""", (file_ID, ))

			if self.cursor.fetchone()[0] == 0:
				self.cursor.execute("""
					DELETE FROM "file"
					WHERE "id" == ?;
				""", (file_ID, ))

			return True

	def relabel_file(self, file_ID, source, destination, expected_file_name):
		# Work around an original bug

		try:
			self.cursor.execute("""PRAGMA "foreign_keys" = 'off';""")

			with savepoint(self.cursor):
				source_IDs = self._get_label_IDs(source)

				if source_IDs is None:
					return False

				source_tag_ID, source_value_ID = source_IDs

				row = self._get_file_by_ID(file_ID)

				if row is None:
					return False

				directory_name, file_name = row

				if file_name != expected_file_name:
					return False

				destination_IDs = self._get_label_IDs(destination)

				if destination_IDs is None:
					return False

				destination_tag_ID, destination_value_ID = destination_IDs

				self.cursor.execute("""
					UPDATE OR REPLACE "file_tag"
					SET "tag_id" = ?, "value_id" = ?
					WHERE "file_id" == ? AND "tag_id" == ? AND "value_id" == ?;
				""", (
					destination_tag_ID, destination_value_ID,
					file_ID,
					source_tag_ID, source_value_ID
				))

				return True
		finally:
			self.cursor.execute("""PRAGMA "foreign_keys" = 'on';""")
