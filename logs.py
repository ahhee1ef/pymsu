import logging
import functools
import errno
import re

def log_calls(function):
	@functools.wraps(function)
	def wrapped(self, *arguments):
		logging.debug("Called %r %r", function.__name__, arguments)

		try:
			result = function(self, *arguments)
		except BaseException as exception:
			logging.error("Exception in a call to %r %r", function.__name__, arguments, exc_info = True)

			raise
		else:
			appendix = ""

			if isinstance(result, int) and -result in errno.errorcode:
				appendix = " ({})".format(errno.errorcode[-result])

			logging.debug("Returned from %r: %r%s", function.__name__, result, appendix)

			return result

	return wrapped

Systemd_levels = {
	logging.CRITICAL: "<2>",
	logging.ERROR: "<3>",
	logging.WARNING: "<4>",
	logging.INFO: "<6>",
	logging.DEBUG: "<7>"
}

class Formatter(logging.Formatter):
	def format(self, record):
		prefix = Systemd_levels[record.levelno]
		message = super().format(record)

		return re.sub("^", prefix, message, flags = re.MULTILINE)

def initialize():
	logging.captureWarnings(True)

	handler = logging.StreamHandler()
	handler.setFormatter(Formatter())

	root_logger = logging.getLogger()
	root_logger.setLevel(logging.INFO)
	root_logger.addHandler(handler)
